use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize, Clone)]
pub struct MasterHost {
    pub port: String,
    pub minecraft_dir: String,
    pub whitelist: Vec<String>,
}
