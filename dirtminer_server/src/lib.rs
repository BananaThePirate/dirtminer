use dirs;
use dirtminer_lib::{net, state};
use host::MasterHost;
use std::net::TcpListener;
use std::{
    collections::HashMap,
    fs::{create_dir_all, read_dir, read_to_string, DirEntry},
    path::PathBuf,
};
use toml;
mod host;
use std::fs::File;
use std::io::Read;

pub fn do_server() -> std::process::ExitCode {
    println!("hosting dirtminer instance");
    let host_master = match get_host_master() {
        Ok(n) => n,
        Err(e) => return e,
    };
    let listener =
        TcpListener::bind("0.0.0.0:".to_string() + &host_master.port).expect("can't host server");
    for stream in listener.incoming() {
        let stream = stream.unwrap();
        let mut stream = net::MasterStream::new(stream);
        loop {
            match stream.host_get_command() {
                net::Command::GetFiles(files) => {
                    let master_files = get_master_files(files, &host_master);
                    stream.host_send_files(master_files);
                },
                net::Command::GetMasterState => {
                    let master_state = get_master_state(&host_master);
                    stream.host_send_state(master_state)
                },
                net::Command::Close => {
                    break;
                }
            }
        }
    }
    std::process::ExitCode::SUCCESS
}

fn get_master_state(host_master: &MasterHost) -> state::State {
    state::State::get_current(
        host_master.minecraft_dir.clone(),
        host_master.whitelist.clone(),
    )
}

fn get_master_files(files: Vec<String>, host_master: &MasterHost) -> HashMap<String, Vec<u8>> {
    let path = PathBuf::from(&host_master.minecraft_dir);
    let mut master_files: HashMap<String, Vec<u8>> = HashMap::new();
    for file_name in files {
        let mut file_path = path.clone();
        file_path.push(&file_name);
        let mut file = File::open(&file_path).unwrap();
        let mut file_buf: Vec<u8> = Vec::new();
        file.read_to_end(&mut file_buf).unwrap();
        master_files.insert(file_name, file_buf);
    }
    master_files
}

fn get_host_master() -> Result<host::MasterHost, std::process::ExitCode> {
    let mut config_dir = dirs::data_local_dir().expect("can't find local config dir");
    config_dir.push("dirtminer");
    config_dir.push("host");
    create_dir_all(&config_dir).expect(&format!(
        "Can't create config dir in {}",
        config_dir.to_str().unwrap()
    ));
    let files: Vec<DirEntry> = read_dir(&config_dir)
        .unwrap()
        .filter(|file| file.as_ref().unwrap().metadata().unwrap().is_file())
        .map(|file| file.unwrap())
        .collect();

    if files.len() == 0 {
        println!(
            "config files not found, made config directory in {}",
            config_dir.display()
        );
        return Err(std::process::ExitCode::SUCCESS);
    }
    let masters: HashMap<String, host::MasterHost> = files
        .into_iter()
        .map(|i| {
            (
                i.file_name().into_string().unwrap(),
                read_to_string(i.path()).unwrap(),
            )
        })
        .map(|(name, data)| {
            (
                name.clone(),
                toml::from_str::<host::MasterHost>(&data)
                    .expect(&format!("Error reading {}", name)),
            )
        })
        .collect();

    let mut data_vec: Vec<host::MasterHost> = Vec::new();
    let mut num: u8 = 1;
    for (name, data) in masters {
        println!("{}) {}", num, name);
        data_vec.push(data);
        num += 1;
    }
    println!("Choose:");
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap();
    let input = input.trim_end();
    let choice: usize = input.parse().unwrap();
    Ok(data_vec[choice - 1].clone())
}
