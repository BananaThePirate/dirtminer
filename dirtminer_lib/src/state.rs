use crc32fast;
use hash_map_diff;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use walkdir::{self, WalkDir};

#[derive(Serialize, Deserialize)]
pub struct State(pub HashMap<String, u32>);

impl State {
    pub fn get_current(minecraft_dir_path: String, include: Vec<String>) -> State {
        State(get_current(minecraft_dir_path, include))
    }

    pub fn compare(&self, other: State) -> Vec<String> {
        let diff = hash_map_diff::hash_map_diff(&self.0, &other.0);
        //todo: allow select deleting of files
        println!(
            "The following files were removed from master :\n{:#?}",
            diff.removed
        );
        println!("the following files will be updated: \n{:#?}", diff.updated);
        let to_download: Vec<String> = diff.updated.into_iter().map(|i| i.0.clone()).collect();
        to_download
    }
}

fn get_whitelisted_dirs(minecraft_dir_path: &str, mut include: Vec<String>) -> Vec<String> {
    for dir in include.clone().iter()
        .map(|dir_path| {
            let mut path = PathBuf::from(&minecraft_dir_path);
            path.push(dir_path);
            path
            })
        .filter(|dir| dir.is_dir())
    {
        let mut files: Vec<String> = WalkDir::new(dir).into_iter()
        .map(|i|  {
            i.unwrap()
            .path()
            .strip_prefix(&minecraft_dir_path)
            .unwrap()
            .display()
            .to_string()
        }).collect();
        include.append(&mut files);
    }
    include
}


fn get_current(minecraft_dir_path: String, include: Vec<String>) -> HashMap<String, u32> {
    let minecraft_dir = WalkDir::new(&minecraft_dir_path).into_iter();
    let include = get_whitelisted_dirs(&minecraft_dir_path, include);
    let necessary_files: Vec<walkdir::DirEntry> = minecraft_dir
        .filter(|i| {
            include.contains(
                &i.as_ref()
                    .unwrap()
                    .path()
                    .strip_prefix(&minecraft_dir_path)
                    .unwrap()
                    .display()
                    .to_string(),
            )
        })
        .map(|i| i.unwrap())
        .collect();
    let mut current_state = HashMap::new();
    for entry in necessary_files {
        let metadata = entry.metadata().unwrap();
        if metadata.is_dir() {}
        if metadata.is_file() {
            let (file_name, file_hash) =
                compute_hash(&entry.path().to_path_buf(), &minecraft_dir_path);
            current_state.insert(file_name, file_hash);
        }
    }
    current_state
}

fn compute_hash(file_path: &PathBuf, minecraft_dir_path: &String) -> (String, u32) {
    let mut file = File::open(&file_path).unwrap();
    let mut file_buf: Vec<u8> = Vec::new();
    file.read_to_end(&mut file_buf).unwrap();
    let file_hash = crc32fast::hash(&file_buf);
    let file_name = &file_path
        .strip_prefix(&minecraft_dir_path)
        .unwrap()
        .display()
        .to_string();
    (file_name.clone(), file_hash)
}
