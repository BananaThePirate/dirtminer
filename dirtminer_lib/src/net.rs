use super::state;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::io::{Read, Write};
use std::net::TcpStream;

#[derive(Serialize, Deserialize)]
pub enum Command {
    GetMasterState,
    GetFiles(Vec<String>),
    Close
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Master {
    pub host: String,
    pub minecraft_dir: String,
}

impl Master {
    pub fn connect(&self) -> MasterStream {
        MasterStream(TcpStream::connect(&self.host).unwrap())
    }
}

pub trait Encode {
    type Input;
    /// Encode data using [bincode] for transport
    fn encode(&self) -> Result<Vec<u8>, Box<bincode::ErrorKind>>;
    /// Decode data using [bincode] after transport
    fn decode(encoded: &[u8]) -> Result<Self::Input, Box<bincode::ErrorKind>>;
}

impl Encode for Command {
    type Input = Command;
    fn encode(&self) -> Result<Vec<u8>, Box<bincode::ErrorKind>> {
        bincode::serialize(&self)
    }
    fn decode(encoded: &[u8]) -> Result<Self::Input, Box<bincode::ErrorKind>> {
        bincode::deserialize(encoded)
    }
}

impl Encode for state::State {
    type Input = state::State;
    fn encode(&self) -> Result<Vec<u8>, Box<bincode::ErrorKind>> {
        bincode::serialize(&self)
    }
    fn decode(encoded: &[u8]) -> Result<Self::Input, Box<bincode::ErrorKind>> {
        bincode::deserialize(encoded)
    }
}

impl Encode for HashMap<String, Vec<u8>> {
    type Input = HashMap<String, Vec<u8>>;
    fn encode(&self) -> Result<Vec<u8>, Box<bincode::ErrorKind>> {
        bincode::serialize(&self)
    }
    fn decode(encoded: &[u8]) -> Result<Self::Input, Box<bincode::ErrorKind>> {
        bincode::deserialize(encoded)
    }
}

pub struct MasterStream(TcpStream);

impl MasterStream {
    pub fn new(stream: TcpStream) -> MasterStream {
        MasterStream(stream)
    }


    //client
    pub fn get_master_state(&mut self) -> state::State {
        self.write(&Command::GetMasterState);
        let master_state: state::State = self.read::<state::State>();
        master_state
    }

    pub fn get_files(&mut self, files: Vec<String>) -> HashMap<String, Vec<u8>> {
        self.write(&Command::GetFiles(files));
        self.read::<HashMap<String, Vec<u8>>>()
    }

    pub fn close_connection(&mut self) {
        self.write(&Command::Close);
    }

    //server

    pub fn host_get_command(&mut self) -> Command {
        self.read::<Command>()
    }

    pub fn host_send_state(&mut self, master_state: state::State) {
        self.write(&master_state);
    }

    pub fn host_send_files(&mut self, files: HashMap<String, Vec<u8>>) {
        self.write(&files);
    }



    fn write<T: Encode>(&mut self, data: &T) {
        let buf = data.encode().expect("Can't encode data");
        self.send(&buf).unwrap();
    }

    fn read<T: Encode>(&mut self) -> T::Input {
        let buf = self.receive().expect("Can't download data");
        T::decode(&buf).expect("Can't deserialize download")
    }

    /// tcp write, but first sends the amount of bytes
    fn send(&mut self, buf: &[u8]) -> Result<(), std::io::Error> {
        self.0.write_all(&(buf.len() as u64).to_be_bytes())?;
        self.0.write_all(buf)
    }
    /// tcp read, but knows the amount of data to read
    fn receive(&mut self) -> Result<Vec<u8>, std::io::Error> {
        let mut size_buf: [u8; 8] = [0; 8]; //u64
        self.0.read_exact(&mut size_buf)?;
        let length = u64::from_be_bytes(size_buf);
        let mut buf = Vec::<u8>::new();
        for _x in 0..(length as usize) {
            buf.push(0);
        }
        self.0.read_exact(&mut buf)?;
        Ok(buf)
    }
}
