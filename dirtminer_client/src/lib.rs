use dirs;
use dirtminer_lib::{net, state};
use std::{
    collections::HashMap,
    fs::{create_dir_all, read_dir, read_to_string, DirEntry, File},
    io::Write,
    path::{PathBuf, self},
};
use toml;

pub fn do_client() -> std::process::ExitCode {
    let master = match get_master() {
        Ok(n) => n,
        Err(e) => return e,
    };
    let mut master_stream = master.connect();
    let master_state = master_stream.get_master_state();
    let include_files: Vec<String> = master_state.0.keys().map(|key| key.to_owned()).collect();
    let current_state = state::State::get_current(master.minecraft_dir.clone(), include_files);
    let to_download = current_state.compare(master_state);
    let files = master_stream.get_files(to_download);

    for (file, data) in files {
        let mut path = PathBuf::from(&master.minecraft_dir);
        path.push(&file);
        // get dir path
/*         match path.parent() {
            None,
            Some(parent) => {
                if parent != Path::from("/")
            }
        } */
        create_dir_all(&path.parent().unwrap()).expect(&format!("failed to create directory for {}", path.display().to_string()));
        if path.is_dir() {
            let is_empty = path.read_dir().unwrap().next().is_none();
            if !is_empty {
                panic!("{} should be a file?", path.display());
            }
            std::fs::remove_dir(&path).unwrap();
        }
        let mut open_file = File::create(path).unwrap();
        open_file
            .write_all(&data)
            .expect(&format!("Failed to write to {}", file));
    }
    master_stream.close_connection();
    println!("successful update, press <enter> to close");

    //stop from closing before user input
    let mut buf = String::new();
    let _ = std::io::stdin().read_line(&mut buf);
    std::process::ExitCode::SUCCESS
}

fn get_master() -> Result<net::Master, std::process::ExitCode> {
    let mut config_dir = dirs::data_local_dir().expect("can't find local config dir");
    config_dir.push("dirtminer");
    create_dir_all(&config_dir).expect(&format!(
        "Can't create config dir in {}",
        config_dir.to_str().unwrap()
    ));
    let files: Vec<DirEntry> = read_dir(&config_dir)
        .unwrap()
        .filter(|file| file.as_ref().unwrap().metadata().unwrap().is_file())
        .map(|file| file.unwrap())
        .collect();

    if files.len() == 0 {
        println!(
            "config files not found, made config directory in {}",
            config_dir.display()
        );
        return Err(std::process::ExitCode::SUCCESS);
    }
    let masters: HashMap<String, net::Master> = files
        .into_iter()
        .map(|i| {
            (
                i.file_name().into_string().unwrap(),
                read_to_string(i.path()).unwrap(),
            )
        })
        .map(|(name, data)| {
            (
                name.clone(),
                toml::from_str::<net::Master>(&data).expect(&format!("Error reading {}", name)),
            )
        })
        .collect();

    let mut data_vec: Vec<net::Master> = Vec::new();
    let mut num: u8 = 1;
    for (name, data) in masters {
        println!("{}) {}", num, name);
        data_vec.push(data);
        num += 1;
    }
    println!("Choose:");
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap();
    let input = input.trim_end();
    let choice: usize = input.parse().unwrap();
    Ok(data_vec[choice - 1].clone())
}
