use dirtminer_client::do_client;
use dirtminer_server::do_server;
fn main() -> std::process::ExitCode {
    let args: Vec<String> = std::env::args().collect();
    if args.len() > 1 {
        if args[1] == "host".to_string() {
            return do_server();
        } else {
            println!("options: \n host\thost modpack for others");
            return std::process::ExitCode::SUCCESS;
        }
    }
    do_client()
}
